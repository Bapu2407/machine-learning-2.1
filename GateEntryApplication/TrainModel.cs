﻿using Microsoft.AspNetCore.Http;
using Microsoft.ML;
using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GateEntryApplication
{
    public class TrainModel
    {
        static readonly string _assetsPath = Path.Combine(Directory.GetCurrentDirectory() + "/wwwroot/assets");
        static readonly string _imagesFolder = Path.Combine(_assetsPath, "images");
        static readonly string _trainTagsTsv = Path.Combine(_imagesFolder, "tags.tsv");
        static readonly string _testTagsTsv = Path.Combine(_imagesFolder, "test-tags.tsv");

        static readonly string _inceptionTensorFlowModel = Path.Combine(_assetsPath, "inception", "tensorflow_inception_graph.pb");
        public ResultDTO TrainModelData()
        {
            try
            {
                //var filepPath = string.Empty;
                //filepPath = $@"/{filename}";
                ////var webRoot = Path.Combine(@"E:\ObjectDetectionConsole\ObjectDetectionConsole\assets", "images");
                //var webRoot = Directory.GetCurrentDirectory() + "/wwwroot/assets/images";
                //using (FileStream fs = System.IO.File.Create(webRoot + filepPath))
                //{
                //    file.CopyTo(fs);
                //    fs.Flush();
                //}
                var res = new ResultDTO();
                MLContext mlContext = new MLContext();
                ITransformer model = GenerateModel(mlContext);
                //var aa = ClassifySingleImage(mlContext, model, filename);
                res.Result = "model update successfully.";
                res.isSuccess = true;
                return res;
            }
            catch (Exception ex)
            {
                var res = new ResultDTO();
                res.Result ="Error----"+ ex.Message +"----Error";
                res.isSuccess = false;
                return res;
            }
        }
        private struct InceptionSettings
        {
            public const int ImageHeight = 224;
            public const int ImageWidth = 224;
            public const float Mean = 117;
            public const float Scale = 1;
            public const bool ChannelsLast = true;
        }
        public static ITransformer GenerateModel(MLContext mlContext)
        {
            IEstimator<ITransformer> pipeline = mlContext.Transforms.LoadImages(outputColumnName: "input", imageFolder: _imagesFolder, inputColumnName: nameof(ImageData.ImagePath))
               // The image transforms transform the images into the model's expected format.
               .Append(mlContext.Transforms.ResizeImages(outputColumnName: "input", imageWidth: InceptionSettings.ImageWidth, imageHeight: InceptionSettings.ImageHeight, inputColumnName: "input"))
               .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "input", interleavePixelColors: InceptionSettings.ChannelsLast, offsetImage: InceptionSettings.Mean))
               .Append(mlContext.Model.LoadTensorFlowModel(_inceptionTensorFlowModel).
   ScoreTensorFlowModel(outputColumnNames: new[] { "softmax2_pre_activation" }, inputColumnNames: new[] { "input" }, addBatchDimensionInput: true))
               .Append(mlContext.Transforms.Conversion.MapValueToKey(outputColumnName: "LabelKey", inputColumnName: "Label"))
               .Append(mlContext.MulticlassClassification.Trainers.LbfgsMaximumEntropy(labelColumnName: "LabelKey", featureColumnName: "softmax2_pre_activation"))
               .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabelValue", "PredictedLabel"))
.AppendCacheCheckpoint(mlContext);
            IDataView trainingData = mlContext.Data.LoadFromTextFile<ImageData>(path: _trainTagsTsv, hasHeader: false);
            ITransformer model = pipeline.Fit(trainingData);
            //For save model start
            using (var stream = System.IO.File.Create("mymodel.zip"))
            {
                mlContext.Model.Save(model, trainingData.Schema, stream);
            }

           // mlContext.Model.Save(model, trainingData.Schema, "model.zip");
            //DataViewSchema modelSchema;
            //ITransformer trainedModel = mlContext.Model.Load("model.zip", out modelSchema);

            //For save model end

            //        IDataView testData = mlContext.Data.LoadFromTextFile<ImageData>(path: _testTagsTsv, hasHeader: false);
            //        IDataView predictions = model.Transform(testData);

            //        // Create an IEnumerable for the predictions for displaying results
            //        IEnumerable<ImagePrediction> imagePredictionData = mlContext.Data.CreateEnumerable<ImagePrediction>(predictions, true);
            //        DisplayResults(imagePredictionData);
            //        MulticlassClassificationMetrics metrics =
            //mlContext.MulticlassClassification.Evaluate(predictions,
            //  labelColumnName: "LabelKey",
            //  predictedLabelColumnName: "PredictedLabel");
            return model;
        }
        public static string ClassifySingleImage(MLContext mlContext, ITransformer model, string filename)
        {
            string _predictSingleImage = Path.Combine(_imagesFolder, filename);
            var imageData = new ImageData()
            {
                ImagePath = _predictSingleImage
            };
            // Make prediction function (input = ImageData, output = ImagePrediction)
            var predictor = mlContext.Model.CreatePredictionEngine<ImageData, ImagePrediction>(model);
            var prediction = predictor.Predict(imageData);
            return $"Image: {Path.GetFileName(imageData.ImagePath)} predicted as: {prediction.PredictedLabelValue} with score: {prediction.Score.Max()} and model saved successfully. ";
            // Console.WriteLine($"Image: {Path.GetFileName(imageData.ImagePath)} predicted as: {prediction.PredictedLabelValue} with score: {prediction.Score.Max()} ");
        }

        private static void DisplayResults(IEnumerable<ImagePrediction> imagePredictionData)
        {
            foreach (ImagePrediction prediction in imagePredictionData)
            {
                Console.WriteLine($"Image: {Path.GetFileName(prediction.ImagePath)} predicted as: {prediction.PredictedLabelValue} with score: {prediction.Score.Max()} ");
            }
        }
        public static IEnumerable<ImageData> ReadFromTsv(string file, string folder)
        {
            return File.ReadAllLines(file)
             .Select(line => line.Split('\t'))
             .Select(line => new ImageData()
             {
                 ImagePath = Path.Combine(folder, line[0])
             });
        }
    }
}
