﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateEntryApplication
{
    public class ResponseDTO
    {
        public float Score { get; set; }
        public string Name { get; set; }
    }
}
