﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GateEntryApplication.Controllers
{
    [Route("api/Trained")]
    [ApiController]
    public class TrainedController : ControllerBase
    {
        // POST api/values
        private IHostingEnvironment _env;

        public TrainedController(IHostingEnvironment env)
        {
            _env = env;
        }
        [HttpGet()]
        public IActionResult Get()
        {
            var tranclas = new TrainModel();
            var modeldata = tranclas.TrainModelData();
            // var path = FileUpload(file);
            return Ok(modeldata);
        }
        // POST api/values

        [HttpPost]
        [Route("UploadImages")]
        public IActionResult UploadImages(string Name,IFormFile file)
        {

            var imageUpload = new FileUpload();
            var webRoot = _env.WebRootPath;
            var imageObj = imageUpload.AddImages(webRoot, Name, file);
            return Ok(imageObj);
        }
    }
}
